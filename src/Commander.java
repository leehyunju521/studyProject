import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import actions.Action;
import actions.user.UserUpdateAction;
import view.View;

public class Commander {
    // 싱글톤 패턴 -> 한 번만 만들어져 전역에서 사용할 클래스
    public static Commander instance;

    // 후입선출 LIFO의 자료구조 -> 히스토리 관리하기에 좋은 자료구조
    private Stack<String> history;

    private Map<String, View> views;
    private Map<String, Action> actions;

    public void setViews(Map<String, View> views) {
        this.views = views;
    }

    public void setActions(Map<String, Action> actions) {
        this.actions = actions;
    }

    public void request(String command) {
        HashMap<String, Action> actions = new HashMap<>();
        actions.put("USER/LIST", new UserUpdateAction());
        // ...

        actions.get(command).execute();
    }

    // 처음에 한번만 인스턴스를 초기화
    private static Commander getInstance() {
        if (instance == null) {
            instance = new Commander();
        }
        return instance;
    }
}
