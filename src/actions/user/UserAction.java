package actions.user;

import actions.Action;
import dao.UserDao;


public abstract class UserAction implements Action {

    // generalization 일반화 : 공통점 찾아서 일반화한 추상클래스 만든다
    protected UserDao userDao;

    // 객체의 생성을 runtime으로 미룬다->lazy initializing
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
