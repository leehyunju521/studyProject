package controller;

import java.util.List;

import dao.UserDao;
import dao.UserFileDao;
import domain.User;
import util.Com;
import util.IOutil;
import view.UserView;

public class SystemController {
    private UserDao userDao;
    UserView userView = new UserView();

    public SystemController() {
        userDao = new UserFileDao();
    }

    public void startSystem() {
        boolean isExit = false;
        do {
            startMenu(userView.selectMenu());
            isExit = userView.isExit();

        } while (!isExit);
    }

    private void startMenu(int type) {

//        IOutil iOutil = new IOutil();

        switch (type) {
            case Com.INSERT_USER:
                userDao.insertUser(userView.createUser());
                break;

            case Com.SEARCH_USER:
                IOutil.show(userDao.serachUser(userView.searchUser()).toString());
                break;

            case Com.UPDATE_USER:
                List<Object> updateData = userView.updateUser();
                userDao.updateUser((String) updateData.get(0), (User) updateData.get(1));
                break;

            case Com.DELETE_USER:
                userDao.deleteUser(userView.deleteUser());
                break;

            case Com.SHOW_ALLUSER:
                IOutil.show(userDao.showAllUser().toString());
                break;

            case Com.DELETE_ALLUSER:
                userDao.deleteAllUser();
                break;
        }
    }

}
