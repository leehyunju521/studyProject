package dao;

import java.util.List;

import domain.User;

/**
 * CRUD
 */
public interface UserDao {
    // create User
    boolean insertUser(User user);

    // read User
    User serachUser(String id);

    // update User
    boolean updateUser(String id, User user);

    // delete User
    boolean deleteUser(String id);

    List<User> showAllUser();

    boolean deleteAllUser();

}
