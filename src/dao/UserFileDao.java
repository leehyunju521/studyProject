package dao;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;

import domain.User;

public class UserFileDao extends UserMemoryDao {
    /*
        constructor -> load from file
        inset,update, delete -> save to file
     */
    public UserFileDao() {
        super();
        loadFromFile();
    }

    private final int byTxtUsingScanner = 1001;
    private final int byTxtUsingBuffered = 1002;
    private final int byDatUsingObject = 1003;

    private boolean loadFromFile() {
        int inputType = byDatUsingObject;

        String pathName = "";
        if (inputType == byDatUsingObject) {
            pathName = "storage.dat";
        } else {
            pathName = "storage.txt";
        }
        File file = new File(pathName);

        // 바이너리 기반
        FileInputStream fileInputStream = null;
        // 텍스트 기반
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;
        // 바이너리 기반 - Object로 가져오기
        BufferedInputStream bufferedInputStream = null;
        ObjectInputStream objectInputStream = null;

        try {
            // 바이너리 기반
            fileInputStream = new FileInputStream(file);
            // 텍스트 기반 - 보조
            inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
            bufferedReader = new BufferedReader(inputStreamReader);
            // 바이너리 기반 - Object로 가져오기
            bufferedInputStream = new BufferedInputStream(fileInputStream);
            objectInputStream = new ObjectInputStream(bufferedInputStream);

            String line = "";
            switch (inputType) {
                case byTxtUsingScanner:
                    Scanner scanner = new Scanner(fileInputStream);
                    while (scanner.hasNextLine()) {
                        line = scanner.nextLine();
                        userList.add(new User().csvToUser(line));
                    }
                    break;

                case byTxtUsingBuffered:
                    while ((line = bufferedReader.readLine()) != null) {
                        userList.add(new User().csvToUser(line));
                    }
                    break;

                case byDatUsingObject:
                    User user = null;
                    // TODO 아마 데이터 끝에 도달했을 때 java.io.EOFException 에러나는듯... 수정 필요
                    while ((user = (User) objectInputStream.readObject()) != null) {
                        userList.add(user);
                    }
                    break;

                default:
                    break;
            }

            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("파일이 존재하지 않음");
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("알수없는 오류");
            return false;
        } finally {
            try {
                objectInputStream.close();
                bufferedInputStream.close();
                bufferedReader.close();
                inputStreamReader.close();
                fileInputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("닫기 실패");
            }
        }

    }

    private boolean saveToFile() {
        int outputType = byDatUsingObject;

        String pathName = "";
        if (outputType == byDatUsingObject) {
            pathName = "storage.dat";
        } else {
            pathName = "storage.txt";
        }
        File file = new File(pathName);

        // 바이너리
        FileOutputStream fileOutputStream = null;
        // 텍스트
        OutputStreamWriter outputStreamWriter = null;
        BufferedWriter bufferedWriter = null;
        // 바이너리 기반 - Object로 저장하기
        BufferedOutputStream bufferedOutputStream = null;
        ObjectOutputStream objectOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            outputStreamWriter = new OutputStreamWriter(fileOutputStream, "UTF-8");
            bufferedWriter = new BufferedWriter(outputStreamWriter);
            bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            objectOutputStream = new ObjectOutputStream(bufferedOutputStream);

            switch (outputType) {
                case byTxtUsingBuffered:
                    for (User user : userList) {
                        bufferedWriter.write(user.userToCsv() + "\r\n");
                    }
                    break;
                case byDatUsingObject:
                    for (User user : userList) {
                        objectOutputStream.writeObject(user);
                    }
                    break;
                default:
                    break;
            }

            return true;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("파일이 존재하지 않음");
            return false;

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("알수없는 오류");
            return false;
        } finally {
            try {
                objectOutputStream.close();
                bufferedOutputStream.close();
                bufferedWriter.close();
                outputStreamWriter.close();
                fileOutputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("닫기 실패");

            }
        }

    }

    @Override
    public boolean insertUser(User user) {
        return super.insertUser(user) && saveToFile();
    }

    @Override
    public boolean updateUser(String id, User user) {
        return super.updateUser(id, user) && saveToFile();
    }

    @Override
    public boolean deleteUser(String id) {
        return super.deleteUser(id) && saveToFile();
    }

    @Override
    public boolean deleteAllUser() {
        return super.deleteAllUser() && saveToFile();
    }
}
