package dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import domain.User;

/**
 * UserDao를 구현한 Concreate Class
 */

// load, save
public class UserMemoryDao implements UserDao {
    protected static List<User> userList;

    public UserMemoryDao() {
        userList = new ArrayList<>();
    }

    @Override
    public boolean insertUser(User user) {
        userList.add(user);
        return true;
    }

    @Override
    public User serachUser(String id) {
        if (getIndex(id) != -1) {
            return userList.get(getIndex(id));
        }
        return null;
    }

    @Override
    public boolean updateUser(String id, User user) {
        if (getIndex(id) != -1) {
            userList.set(getIndex(id), user);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteUser(String id) {
        if (getIndex(id) != -1) {
            userList.remove(getIndex(id));
            return true;
        }
        return false;
    }


    @Override
    public List<User> showAllUser() {
        sortUserList();
        return userList;
    }

    private void sortUserList() {
        Collections.sort(userList, new Comparator<User>() {
            @Override
            public int compare(User user1, User user2) {
                return user1.compareTo(user2);
            }
        });
    }

    @Override
    public boolean deleteAllUser() {
        userList.clear();
        return true;
    }

    private int getIndex(String id) {
        for (int i = 0; i < userList.size(); i++) {
            if (id.equals(userList.get(i).getId())) {
                return i;
            }
        }
        return -1;
    }


}
