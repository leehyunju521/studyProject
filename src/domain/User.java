package domain;

import java.io.Serializable;
import java.util.Comparator;

public class User implements Comparable<User>, Serializable {
    private String id;
    private String name;
    private String age;


    public User() {
    }

    public User(String id, String name, String age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String getId() {
        return id;
    }

    // MethodChaning 디자인 패턴
    public User setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getAge() {
        return age;
    }

    public User setAge(String age) {
        this.age = age;
        return this;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (!(o instanceof User))
            return false;

        User user = (User) o;
        if (this.id.equals(user.getId()) &&
                this.name.equals(user.getName()) &&
                this.age.equals(user.getAge())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        String str = id + name + age;
        return str.hashCode();
    }

    public User csvToUser(String csv) {
        String[] field = csv.split(",");
        id = field[0];
        name = field[1];
        age = field[2];

        return this;
    }

    public String userToCsv() {
        return id + ","
                + name + ","
                + age;
    }


    // 방법1
    @Override
    public int compareTo(User user) {
        if (Integer.parseInt(this.age) == Integer.parseInt(user.getAge())) {
            return 0;
        } else if (Integer.parseInt(this.age) < Integer.parseInt(user.getAge())) {
            return -1;
        } else {
            return 1;
        }
    }

    // 방법2
    public static class byName implements Comparator<User> {
        @Override
        public int compare(User o1, User o2) {
            return 0;
        }
    }

    public static class byId implements Comparator<User> {
        @Override
        public int compare(User o1, User o2) {
            return 0;
        }
    }

}
