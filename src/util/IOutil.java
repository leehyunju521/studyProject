package util;

import java.util.Scanner;

public  class IOutil {
    private static Scanner scanner;

    static  {
        scanner = new Scanner(System.in);
    }

    public static void show(String str) {
        if (str.isEmpty())
            return;
        System.out.println(str);
    }

    public static String showAndRead(String str) {
        if (str.isEmpty())
            return "";
        System.out.println(str);
        return scanner.next();
    }

}