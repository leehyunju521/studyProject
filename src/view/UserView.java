package view;

import java.util.ArrayList;
import java.util.List;

import domain.User;
import util.Com;
import util.IOutil;

public class UserView {
    // 리팩토링 필요
    public void render() {
        while (true) {
            IOutil.show("------시스템 시작------");
            IOutil.show("사용자 등록 -> 1");
            IOutil.show("사용자 검색 -> 2");
            IOutil.show("사용자 업데이트 -> 3");
            IOutil.show("사용자 삭제 -> 4");

            String[] commands = {
                    "USER/ADD",
                    "USER/SEARCH",
                    "USER/UPDATE",
                    "USER/DELETE"
            };
        }
    }

    public int selectMenu() {
        IOutil.show("------시스템 시작------");
        IOutil.show("사용자 등록 -> 1");
        IOutil.show("사용자 검색 -> 2");
        IOutil.show("사용자 업데이트 -> 3");
        IOutil.show("사용자 삭제 -> 4");

        int type = Integer.parseInt(IOutil.showAndRead("원하는 기능의 번호를 입력해주세요."));
        switch (type) {
            case 1:
                return Com.INSERT_USER;
            case 3:
                return Com.UPDATE_USER;
            case 2:
                IOutil.show("아이디로 검색 -> 1");
                IOutil.show("전체 사용자 검색 -> 2");
                return Integer.parseInt(IOutil.showAndRead("원하는 기능의 번호를 입력해주세요.")) == 2 ? Com.SHOW_ALLUSER : Com.SEARCH_USER;
            case 4:
                IOutil.show("아이디로 삭제 -> 1");
                IOutil.show("전체 사용자 삭제 -> 2");
                return Integer.parseInt(IOutil.showAndRead("원하는 기능의 번호를 입력해주세요.")) == 2 ? Com.DELETE_ALLUSER : Com.DELETE_USER;
            default:
                break;
        }

        return -1;
    }


    public User createUser() {
        IOutil.show("------사용자 등록------");
        return new User(
                IOutil.showAndRead("아이디를 입력해주세요."),
                IOutil.showAndRead("이름을 입력해주세요."),
                IOutil.showAndRead("나이를 입력해주세요.")
        );
    }

    public String searchUser() {
        IOutil.show("------사용자 검색------");
        return IOutil.showAndRead("아이디를 입력해주세요.");
    }

    public List<Object> updateUser() {
        IOutil.show("------사용자 업데이트------");
        List<Object> updateData = new ArrayList<Object>();
        updateData.add(IOutil.showAndRead("업데이트할 아이디를 입력해주세요."));
        updateData.add(new User(
                IOutil.showAndRead("새로운 아이디를 입력해주세요."),
                IOutil.showAndRead("새로운 이름을 입력해주세요."),
                IOutil.showAndRead("새로운 나이를 입력해주세요.")));

        return updateData;
    }

    public String deleteUser() {
        IOutil.show("------사용자 삭제------");
        return IOutil.showAndRead("삭제할 아이디를 입력해주세요.");
    }

    public boolean isExit() {
        IOutil.show("사용자 시스템을 계속 사용하시겠습니까?");
        IOutil.show("네 -> 1");
        IOutil.show("아니오(시스템 종료) -> 2");

        if ("1".equals(IOutil.showAndRead("원하는 기능의 번호를 입력해주세요."))) {
            return false;
        }

        return true;
    }

    // User 정렬
    private void sortUser() {
    }

}
